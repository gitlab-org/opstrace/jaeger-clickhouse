package clickhousespanstore

import (
	"context"
	"database/sql/driver"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"math"
	"strings"
	"testing"
	"time"

	sqlmock "github.com/DATA-DOG/go-sqlmock"
	"github.com/gogo/protobuf/proto"
	"github.com/jaegertracing/jaeger/model"
	"github.com/jaegertracing/jaeger/storage/spanstore"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"github.com/jaegertracing/jaeger-clickhouse/storage/clickhousespanstore/mocks"
)

const (
	testOperationsTable = "test_operations_table"
	testNumTraces       = 10
	testSpansInTrace    = 2
	testMaxNumSpans     = 0
	expandTraceID       = true
)

var spyLogger = mocks.NewSpyLogger()

var testStartTime = time.Date(2010, 3, 15, 7, 40, 0, 0, time.UTC)

func TestTraceReader_FindTraceIDs(t *testing.T) {
	service := "service"

	tests := map[string]struct {
		queryTemplate string
		firstArgs     []driver.Value
		tenant        string
	}{
		"default": {
			queryTemplate: "SELECT DISTINCT traceID FROM %s WHERE service = ? AND timestamp >= ? AND timestamp <= ?%s ORDER BY service, timestamp DESC LIMIT ?",
			firstArgs:     []driver.Value{service},
		},
		"tenant": {
			queryTemplate: "SELECT DISTINCT traceID FROM %s WHERE service = ? AND tenant = ? AND timestamp >= ? AND timestamp <= ?%s ORDER BY service, timestamp DESC LIMIT ?",
			firstArgs:     []driver.Value{service, testTenant},
			tenant:        testTenant,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			db, mock, err := mocks.GetDbMock()
			require.NoError(t, err, "an error was not expected when opening a stub database connection")
			defer db.Close()

			traceReader := NewTraceReader(spyLogger, db, testOperationsTable, testIndexTable, testSpansTable, test.tenant, testMaxNumSpans, expandTraceID)
			start := testStartTime
			end := start.Add(24 * time.Hour)
			fullDuration := end.Sub(start)
			duration := fullDuration
			for i := 0; i < maxProgressiveSteps; i++ {
				duration /= 2
			}
			params := spanstore.TraceQueryParameters{
				ServiceName:  service,
				NumTraces:    testNumTraces,
				StartTimeMin: start,
				StartTimeMax: end,
			}

			expectedTraceIDs := make([]model.TraceID, testNumTraces)
			traceIDValues := make([]driver.Value, testNumTraces)
			for i := range expectedTraceIDs {
				traceID := model.TraceID{Low: uint64(i)}
				expectedTraceIDs[i] = traceID
				traceIDValues[i] = traceID.String()
			}

			found := traceIDValues[:0]
			endArg := end
			for i := 0; i < maxProgressiveSteps; i++ {
				if i == maxProgressiveSteps-1 {
					duration = fullDuration
				}

				startArg := endArg.Add(-duration)
				if startArg.Before(start) {
					startArg = start
				}

				// Select how many spans query will return
				index := int(math.Min(float64(i*2+1), testNumTraces))
				if i == maxProgressiveSteps-1 {
					index = testNumTraces
				}
				args := test.firstArgs
				args = append(args, startArg)
				args = append(args, endArg)
				args = append(args, found...)
				args = append(args, testNumTraces-len(found))
				mock.
					ExpectQuery(fmt.Sprintf(
						test.queryTemplate,
						testIndexTable,
						func() string {
							if len(found) == 0 {
								return ""
							}
							return " AND traceID NOT IN (?" + strings.Repeat(",?", len(found)-1) + ")"
						}(),
					)).
					WithArgs(args...).
					WillReturnRows(getRows(traceIDValues[len(found):index]))
				endArg = startArg
				duration *= 2
				found = traceIDValues[:index]
			}

			traceIDs, err := traceReader.FindTraceIDs(context.Background(), &params)
			require.NoError(t, err)
			assert.Equal(t, expectedTraceIDs, traceIDs)
			assert.NoError(t, mock.ExpectationsWereMet())
		})
	}
}

func TestTraceReader_FindTraceIDsShortDurationAfterReduction(t *testing.T) {
	db, mock, err := mocks.GetDbMock()
	require.NoError(t, err, "an error was not expected when opening a stub database connection")
	defer db.Close()

	traceReader := NewTraceReader(spyLogger, db, testOperationsTable, testIndexTable, testSpansTable, "", testMaxNumSpans, expandTraceID)
	service := "service"
	start := testStartTime
	end := start.Add(8 * time.Hour)
	fullDuration := end.Sub(start)
	duration := minTimespanForProgressiveSearch
	params := spanstore.TraceQueryParameters{
		ServiceName:  service,
		NumTraces:    testNumTraces,
		StartTimeMin: start,
		StartTimeMax: end,
	}

	expectedTraceIDs := make([]model.TraceID, testNumTraces)
	traceIDValues := make([]driver.Value, testNumTraces)
	for i := range expectedTraceIDs {
		traceID := model.TraceID{Low: uint64(i)}
		expectedTraceIDs[i] = traceID
		traceIDValues[i] = traceID.String()
	}

	found := traceIDValues[:0]
	endArg := end
	for i := 0; i < maxProgressiveSteps; i++ {
		if i == maxProgressiveSteps-1 {
			duration = fullDuration
		}

		startArg := endArg.Add(-duration)
		if startArg.Before(start) {
			startArg = start
		}

		index := func() int {
			switch i {
			case 0:
				return 1
			case 1:
				return 3
			case 2:
				return 5
			default:
				return testNumTraces
			}
		}()
		args := append(
			append(
				[]driver.Value{
					service,
					startArg,
					endArg,
				},
				found...),
			testNumTraces-len(found))
		mock.
			ExpectQuery(fmt.Sprintf(
				"SELECT DISTINCT traceID FROM %s WHERE service = ? AND timestamp >= ? AND timestamp <= ?%s ORDER BY service, timestamp DESC LIMIT ?",
				testIndexTable,
				func() string {
					if len(found) == 0 {
						return ""
					}
					return " AND traceID NOT IN (?" + strings.Repeat(",?", len(found)-1) + ")"
				}(),
			)).
			WithArgs(args...).
			WillReturnRows(getRows(traceIDValues[len(found):index]))
		endArg = startArg
		duration *= 2
		found = traceIDValues[:index]
	}

	traceIDs, err := traceReader.FindTraceIDs(context.Background(), &params)
	require.NoError(t, err)
	assert.Equal(t, expectedTraceIDs, traceIDs)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestTraceReader_FindTraceIDsEarlyExit(t *testing.T) {
	db, mock, err := mocks.GetDbMock()
	require.NoError(t, err, "an error was not expected when opening a stub database connection")
	defer db.Close()

	traceReader := NewTraceReader(spyLogger, db, testOperationsTable, testIndexTable, testSpansTable, "", testMaxNumSpans, expandTraceID)
	service := "service"
	start := testStartTime
	end := start.Add(24 * time.Hour)
	duration := end.Sub(start)
	for i := 0; i < maxProgressiveSteps; i++ {
		duration /= 2
	}
	params := spanstore.TraceQueryParameters{
		ServiceName:  service,
		NumTraces:    testNumTraces,
		StartTimeMin: start,
		StartTimeMax: end,
	}

	expectedTraceIDs := make([]model.TraceID, testNumTraces)
	traceIDValues := make([]driver.Value, testNumTraces)
	for i := range expectedTraceIDs {
		traceID := model.TraceID{Low: uint64(i)}
		expectedTraceIDs[i] = traceID
		traceIDValues[i] = traceID.String()
	}

	endArg := end
	startArg := endArg.Add(-duration)
	if startArg.Before(start) {
		startArg = start
	}

	mock.
		ExpectQuery(fmt.Sprintf(
			"SELECT DISTINCT traceID FROM %s WHERE service = ? AND timestamp >= ? AND timestamp <= ? ORDER BY service, timestamp DESC LIMIT ?",
			testIndexTable,
		)).
		WithArgs(
			service,
			startArg,
			endArg,
			testNumTraces,
		).
		WillReturnRows(getRows(traceIDValues))

	traceIDs, err := traceReader.FindTraceIDs(context.Background(), &params)
	require.NoError(t, err)
	assert.Equal(t, expectedTraceIDs, traceIDs)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestTraceReader_FindTraceIDsShortRange(t *testing.T) {
	db, mock, err := mocks.GetDbMock()
	require.NoError(t, err, "an error was not expected when opening a stub database connection")
	defer db.Close()

	traceReader := NewTraceReader(spyLogger, db, testOperationsTable, testIndexTable, testSpansTable, "", testMaxNumSpans, expandTraceID)
	service := "service"
	start := testStartTime
	end := start.Add(time.Hour)
	params := spanstore.TraceQueryParameters{
		ServiceName:  service,
		NumTraces:    testNumTraces,
		StartTimeMin: start,
		StartTimeMax: end,
	}

	expectedTraceIDs := make([]model.TraceID, testNumTraces)
	traceIDValues := make([]driver.Value, testNumTraces)
	for i := range expectedTraceIDs {
		traceID := model.TraceID{Low: uint64(i)}
		expectedTraceIDs[i] = traceID
		traceIDValues[i] = traceID.String()
	}

	mock.
		ExpectQuery(fmt.Sprintf(
			"SELECT DISTINCT traceID FROM %s WHERE service = ? AND timestamp >= ? AND timestamp <= ? ORDER BY service, timestamp DESC LIMIT ?",
			testIndexTable,
		)).
		WithArgs(
			service,
			start,
			end,
			testNumTraces,
		).
		WillReturnRows(getRows(traceIDValues))

	traceIDs, err := traceReader.FindTraceIDs(context.Background(), &params)
	require.NoError(t, err)
	assert.Equal(t, expectedTraceIDs, traceIDs)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestTraceReader_FindTraceIDsQueryError(t *testing.T) {
	db, mock, err := mocks.GetDbMock()
	require.NoError(t, err, "an error was not expected when opening a stub database connection")
	defer db.Close()

	traceReader := NewTraceReader(spyLogger, db, testOperationsTable, testIndexTable, testSpansTable, "", testMaxNumSpans, expandTraceID)
	service := "service"
	start := testStartTime
	end := start.Add(24 * time.Hour)
	duration := end.Sub(start)
	for i := 0; i < maxProgressiveSteps; i++ {
		duration /= 2
	}
	params := spanstore.TraceQueryParameters{
		ServiceName:  service,
		NumTraces:    testNumTraces,
		StartTimeMin: start,
		StartTimeMax: end,
	}

	mock.
		ExpectQuery(fmt.Sprintf(
			"SELECT DISTINCT traceID FROM %s WHERE service = ? AND timestamp >= ? AND timestamp <= ? ORDER BY service, timestamp DESC LIMIT ?",
			testIndexTable,
		)).
		WithArgs(
			service,
			end.Add(-duration),
			end,
			testNumTraces,
		).
		WillReturnError(errorMock)

	traceIDs, err := traceReader.FindTraceIDs(context.Background(), &params)
	require.ErrorIs(t, err, errorMock)
	assert.Equal(t, []model.TraceID(nil), traceIDs)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestTraceReader_FindTraceIDsZeroStartTime(t *testing.T) {
	db, mock, err := mocks.GetDbMock()
	require.NoError(t, err, "an error was not expected when opening a stub database connection")
	defer db.Close()

	traceReader := NewTraceReader(spyLogger, db, testOperationsTable, testIndexTable, testSpansTable, "", testMaxNumSpans, expandTraceID)
	service := "service"
	start := time.Time{}
	end := testStartTime
	params := spanstore.TraceQueryParameters{
		ServiceName:  service,
		NumTraces:    testNumTraces,
		StartTimeMin: start,
		StartTimeMax: end,
	}

	traceIDs, err := traceReader.FindTraceIDs(context.Background(), &params)
	require.ErrorIs(t, err, errStartTimeRequired)
	assert.Equal(t, []model.TraceID(nil), traceIDs)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestTraceReader_GetServices(t *testing.T) {
	tests := map[string]struct {
		query  string
		args   []driver.Value
		tenant string
	}{
		"default": {
			query: fmt.Sprintf("SELECT service FROM %s GROUP BY service", testOperationsTable),
			args:  []driver.Value{},
		},
		"tenant": {
			query:  fmt.Sprintf("SELECT service FROM %s WHERE tenant = ? GROUP BY service", testOperationsTable),
			args:   []driver.Value{testTenant},
			tenant: testTenant,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			db, mock, err := mocks.GetDbMock()
			require.NoError(t, err, "an error was not expected when opening a stub database connection")
			defer db.Close()

			traceReader := NewTraceReader(spyLogger, db, testOperationsTable, testIndexTable, testSpansTable, test.tenant, testMaxNumSpans, expandTraceID)
			expectedServices := []string{"GET /first", "POST /second", "PUT /third"}
			expectedServiceValues := make([]driver.Value, len(expectedServices))
			for i := range expectedServices {
				expectedServiceValues[i] = expectedServices[i]
			}
			queryResult := getRows(expectedServiceValues)

			mock.ExpectQuery(test.query).WithArgs(test.args...).WillReturnRows(queryResult)

			services, err := traceReader.GetServices(context.Background())
			require.NoError(t, err)
			assert.Equal(t, expectedServices, services)
			assert.NoError(t, mock.ExpectationsWereMet())
		})
	}
}

func TestTraceReader_GetServicesQueryError(t *testing.T) {
	db, mock, err := mocks.GetDbMock()
	require.NoError(t, err, "an error was not expected when opening a stub database connection")
	defer db.Close()

	traceReader := NewTraceReader(spyLogger, db, testOperationsTable, testIndexTable, testSpansTable, "", testMaxNumSpans, expandTraceID)

	mock.
		ExpectQuery(fmt.Sprintf("SELECT service FROM %s GROUP BY service", testOperationsTable)).
		WillReturnError(errorMock)
	services, err := traceReader.GetServices(context.Background())
	require.ErrorIs(t, err, errorMock)
	assert.Equal(t, []string(nil), services)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestTraceReader_GetServicesNoTable(t *testing.T) {
	db, _, err := mocks.GetDbMock()
	require.NoError(t, err, "an error was not expected when opening a stub database connection")
	defer db.Close()

	traceReader := NewTraceReader(spyLogger, db, "", testIndexTable, testSpansTable, "", testMaxNumSpans, expandTraceID)

	services, err := traceReader.GetServices(context.Background())
	require.ErrorIs(t, err, errNoOperationsTable)
	assert.Equal(t, []string(nil), services)
}

func TestTraceReader_GetOperations(t *testing.T) {
	db, mock, err := mocks.GetDbMock()
	require.NoError(t, err, "an error was not expected when opening a stub database connection")
	defer db.Close()

	service := "test service"
	params := spanstore.OperationQueryParameters{ServiceName: service}
	tests := map[string]struct {
		tenant   string
		query    string
		args     []driver.Value
		rows     *sqlmock.Rows
		expected []spanstore.Operation
	}{
		"default": {
			query: fmt.Sprintf("SELECT operation, spankind FROM %s WHERE service = ? GROUP BY operation, spankind ORDER BY operation", testOperationsTable),
			args:  []driver.Value{service},
			rows: sqlmock.NewRows([]string{"operation", "spankind"}).
				AddRow("operation_1", "client").
				AddRow("operation_2", ""),
			expected: []spanstore.Operation{{Name: "operation_1", SpanKind: "client"}, {Name: "operation_2"}},
		},
		"tenant": {
			tenant: testTenant,
			query:  fmt.Sprintf("SELECT operation, spankind FROM %s WHERE tenant = ? AND service = ? GROUP BY operation, spankind ORDER BY operation", testOperationsTable),
			args:   []driver.Value{testTenant, service},
			rows: sqlmock.NewRows([]string{"operation", "spankind"}).
				AddRow("operation_1", "client").
				AddRow("operation_2", ""),
			expected: []spanstore.Operation{{Name: "operation_1", SpanKind: "client"}, {Name: "operation_2"}},
		},
	}
	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			mock.
				ExpectQuery(test.query).
				WithArgs(test.args...).
				WillReturnRows(test.rows)

			traceReader := NewTraceReader(spyLogger, db, testOperationsTable, testIndexTable, testSpansTable, test.tenant, testMaxNumSpans, expandTraceID)
			operations, err := traceReader.GetOperations(context.Background(), params)
			require.NoError(t, err)
			assert.Equal(t, test.expected, operations)
			assert.NoError(t, mock.ExpectationsWereMet())
		})
	}
}

func TestTraceReader_GetOperationsQueryError(t *testing.T) {
	db, mock, err := mocks.GetDbMock()
	require.NoError(t, err, "an error was not expected when opening a stub database connection")
	defer db.Close()

	traceReader := NewTraceReader(spyLogger, db, testOperationsTable, testIndexTable, testSpansTable, "", testMaxNumSpans, expandTraceID)
	service := "test service"
	params := spanstore.OperationQueryParameters{ServiceName: service}
	mock.
		ExpectQuery(fmt.Sprintf("SELECT operation, spankind FROM %s WHERE service = ? GROUP BY operation, spankind ORDER BY operation", testOperationsTable)).
		WithArgs(service).
		WillReturnError(errorMock)

	operations, err := traceReader.GetOperations(context.Background(), params)
	assert.ErrorIs(t, err, errorMock)
	assert.Equal(t, []spanstore.Operation(nil), operations)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestTraceReader_GetOperationsNoTable(t *testing.T) {
	db, _, err := mocks.GetDbMock()
	require.NoError(t, err, "an error was not expected when opening a stub database connection")
	defer db.Close()

	traceReader := NewTraceReader(spyLogger, db, "", testIndexTable, testSpansTable, "", testMaxNumSpans, expandTraceID)
	service := "test service"
	params := spanstore.OperationQueryParameters{ServiceName: service}
	operations, err := traceReader.GetOperations(context.Background(), params)
	assert.ErrorIs(t, err, errNoOperationsTable)
	assert.Equal(t, []spanstore.Operation(nil), operations)
}

func TestTraceReader_GetTrace(t *testing.T) {
	db, mock, err := mocks.GetDbMock()
	require.NoError(t, err, "an error was not expected when opening a stub database connection")
	defer db.Close()

	traceID := model.TraceID{High: 0, Low: 1}
	spanRefs := generateRandomSpans(testSpansInTrace)
	trace := model.Trace{}
	for _, span := range spanRefs {
		span.TraceID = traceID
		trace.Spans = append(trace.Spans, span)
	}
	spans := make([]model.Span, len(spanRefs))
	for i := range spanRefs {
		spans[i] = *spanRefs[i]
	}

	tests := map[string]struct {
		tenant        string
		queryResult   *sqlmock.Rows
		expectedTrace *model.Trace
		expectedError error
	}{
		"json": {
			queryResult:   getEncodedSpans(spans, func(span *model.Span) ([]byte, error) { return json.Marshal(span) }),
			expectedTrace: &trace,
			expectedError: nil,
		},
		"json tenant": {
			tenant:        testTenant,
			queryResult:   getEncodedSpans(spans, func(span *model.Span) ([]byte, error) { return json.Marshal(span) }),
			expectedTrace: &trace,
			expectedError: nil,
		},
		"protobuf": {
			queryResult:   getEncodedSpans(spans, func(span *model.Span) ([]byte, error) { return proto.Marshal(span) }),
			expectedTrace: &trace,
			expectedError: nil,
		},
		"protobuf tenant": {
			tenant:        testTenant,
			queryResult:   getEncodedSpans(spans, func(span *model.Span) ([]byte, error) { return proto.Marshal(span) }),
			expectedTrace: &trace,
			expectedError: nil,
		},
		"trace not found": {
			queryResult:   sqlmock.NewRows([]string{"model"}),
			expectedTrace: nil,
			expectedError: spanstore.ErrTraceNotFound,
		},
		"query error": {
			queryResult:   getEncodedSpans(spans, func(span *model.Span) ([]byte, error) { return json.Marshal(span) }).RowError(0, errorMock),
			expectedTrace: nil,
			expectedError: errorMock,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			if test.tenant == "" {
				mock.
					ExpectQuery(
						fmt.Sprintf("SELECT model FROM %s PREWHERE traceID IN (?)", testSpansTable),
					).
					WithArgs(traceID).
					WillReturnRows(test.queryResult)
			} else {
				mock.
					ExpectQuery(
						fmt.Sprintf("SELECT model FROM %s PREWHERE traceID IN (?) AND tenant = ?", testSpansTable),
					).
					WithArgs(traceID, test.tenant).
					WillReturnRows(test.queryResult)
			}

			traceReader := NewTraceReader(spyLogger, db, testOperationsTable, testIndexTable, testSpansTable, test.tenant, testMaxNumSpans, expandTraceID)
			trace, err := traceReader.GetTrace(context.Background(), traceID)
			require.ErrorIs(t, err, test.expectedError)
			if trace != nil {
				model.SortTrace(trace)
			}
			if test.expectedTrace != nil {
				model.SortTrace(test.expectedTrace)
			}
			assert.Equal(t, test.expectedTrace, trace)
			assert.NoError(t, mock.ExpectationsWereMet())
		})
	}
}

func TestSpanWriter_getTraces(t *testing.T) {
	db, mock, err := mocks.GetDbMock()
	require.NoError(t, err, "an error was not expected when opening a stub database connection")
	defer db.Close()

	traceIDs := []model.TraceID{
		{High: 0, Low: 1},
		{High: 2, Low: 2},
		{High: 1, Low: 3},
		{High: 0, Low: 4},
	}
	spans := make([]model.Span, testSpansInTrace*len(traceIDs))
	for i := 0; i < testSpansInTrace*len(traceIDs); i++ {
		traceID := traceIDs[i%len(traceIDs)]
		spans[i] = generateRandomSpan()
		spans[i].TraceID = traceID
	}

	traceIDStrings := make([]driver.Value, 4)
	for i, traceID := range traceIDs {
		traceIDStrings[i] = traceID.String()
	}
	traceIDHexes := make([]driver.Value, 4)
	for i, traceID := range traceIDs {
		bts, _ := traceIDToBytes(traceID)
		traceIDHexes[i] = hex.EncodeToString(bts)
	}

	defaultQuery := fmt.Sprintf("SELECT model FROM %s PREWHERE traceID IN (?,?,?,?)", testSpansTable)
	tenantQuery := fmt.Sprintf("SELECT model FROM %s PREWHERE traceID IN (?,?,?,?) AND tenant = ?", testSpansTable)

	defaultQueryNoExpand := fmt.Sprintf("SELECT model FROM %s PREWHERE lower(hex(traceID)) IN (?,?,?,?)", testSpansTable)
	tenantQueryNoExpand := fmt.Sprintf("SELECT model FROM %s PREWHERE lower(hex(traceID)) IN (?,?,?,?) AND tenant = ?", testSpansTable)

	tests := map[string]struct {
		tenant         string
		query          string
		args           []driver.Value
		queryResult    *sqlmock.Rows
		expectedTraces []*model.Trace
		expandTraceID  bool
	}{
		"JSON encoded traces one span per trace": {
			query:          defaultQuery,
			args:           traceIDStrings,
			queryResult:    getEncodedSpans(spans[:len(traceIDs)], func(span *model.Span) ([]byte, error) { return json.Marshal(span) }),
			expectedTraces: getTracesFromSpans(spans[:len(traceIDs)]),
			expandTraceID:  expandTraceID,
		},
		"tenant JSON encoded traces one span per trace": {
			tenant:         testTenant,
			query:          tenantQuery,
			args:           append(traceIDStrings, testTenant),
			queryResult:    getEncodedSpans(spans[:len(traceIDs)], func(span *model.Span) ([]byte, error) { return json.Marshal(span) }),
			expectedTraces: getTracesFromSpans(spans[:len(traceIDs)]),
			expandTraceID:  expandTraceID,
		},
		"Protobuf encoded traces one span per trace": {
			query:          defaultQuery,
			args:           traceIDStrings,
			queryResult:    getEncodedSpans(spans[:len(traceIDs)], func(span *model.Span) ([]byte, error) { return proto.Marshal(span) }),
			expectedTraces: getTracesFromSpans(spans[:len(traceIDs)]),
			expandTraceID:  expandTraceID,
		},
		"Protobuf encoded traces one span per trace no expand": {
			query:          defaultQueryNoExpand,
			args:           traceIDHexes,
			queryResult:    getEncodedSpans(spans[:len(traceIDs)], func(span *model.Span) ([]byte, error) { return proto.Marshal(span) }),
			expectedTraces: getTracesFromSpans(spans[:len(traceIDs)]),
			expandTraceID:  !expandTraceID,
		},
		"tenant Protobuf encoded traces one span per trace": {
			tenant:         testTenant,
			query:          tenantQuery,
			args:           append(traceIDStrings, testTenant),
			queryResult:    getEncodedSpans(spans[:len(traceIDs)], func(span *model.Span) ([]byte, error) { return proto.Marshal(span) }),
			expectedTraces: getTracesFromSpans(spans[:len(traceIDs)]),
			expandTraceID:  expandTraceID,
		},
		"JSON encoded traces many spans per trace": {
			query:          defaultQuery,
			args:           traceIDStrings,
			queryResult:    getEncodedSpans(spans, func(span *model.Span) ([]byte, error) { return json.Marshal(span) }),
			expectedTraces: getTracesFromSpans(spans),
			expandTraceID:  expandTraceID,
		},
		"tenant JSON encoded traces many spans per trace": {
			tenant:         testTenant,
			query:          tenantQuery,
			args:           append(traceIDStrings, testTenant),
			queryResult:    getEncodedSpans(spans, func(span *model.Span) ([]byte, error) { return json.Marshal(span) }),
			expectedTraces: getTracesFromSpans(spans),
			expandTraceID:  expandTraceID,
		},
		"Protobuf encoded traces many spans per trace": {
			query:          defaultQuery,
			args:           traceIDStrings,
			queryResult:    getEncodedSpans(spans, func(span *model.Span) ([]byte, error) { return proto.Marshal(span) }),
			expectedTraces: getTracesFromSpans(spans),
			expandTraceID:  expandTraceID,
		},
		"tenant Protobuf encoded traces many spans per trace": {
			tenant:         testTenant,
			query:          tenantQuery,
			args:           append(traceIDStrings, testTenant),
			queryResult:    getEncodedSpans(spans, func(span *model.Span) ([]byte, error) { return proto.Marshal(span) }),
			expectedTraces: getTracesFromSpans(spans),
			expandTraceID:  expandTraceID,
		},
		"tenant Protobuf encoded traces many spans per trace no expand": {
			tenant:         testTenant,
			query:          tenantQueryNoExpand,
			args:           append(traceIDHexes, testTenant),
			queryResult:    getEncodedSpans(spans, func(span *model.Span) ([]byte, error) { return proto.Marshal(span) }),
			expectedTraces: getTracesFromSpans(spans),
			expandTraceID:  !expandTraceID,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			mock.
				ExpectQuery(test.query).
				WithArgs(test.args...).
				WillReturnRows(test.queryResult)

			traceReader := NewTraceReader(spyLogger, db, testOperationsTable, testIndexTable, testSpansTable, test.tenant, testMaxNumSpans, test.expandTraceID)
			traces, err := traceReader.getTraces(context.Background(), traceIDs)
			require.NoError(t, err)
			model.SortTraces(traces)
			assert.Equal(t, test.expectedTraces, traces)
			assert.NoError(t, mock.ExpectationsWereMet())
		})
	}
}

func TestSpanWriter_getTracesIncorrectData(t *testing.T) {
	db, mock, err := mocks.GetDbMock()
	require.NoError(t, err, "an error was not expected when opening a stub database connection")
	defer db.Close()

	traceIDs := []model.TraceID{
		{High: 0, Low: 1},
		{High: 2, Low: 2},
		{High: 1, Low: 3},
		{High: 0, Low: 4},
	}
	spans := make([]model.Span, 2*len(traceIDs))
	for i := 0; i < 2*len(traceIDs); i++ {
		traceID := traceIDs[i%len(traceIDs)]
		spans[i] = generateRandomSpan()
		spans[i].TraceID = traceID
	}

	traceIDStrings := make([]driver.Value, 4)
	for i, traceID := range traceIDs {
		traceIDStrings[i] = traceID.String()
	}

	defaultQuery := fmt.Sprintf("SELECT model FROM %s PREWHERE traceID IN (?,?,?,?)", testSpansTable)
	tenantQuery := fmt.Sprintf("SELECT model FROM %s PREWHERE traceID IN (?,?,?,?) AND tenant = ?", testSpansTable)

	tests := map[string]struct {
		tenant         string
		query          string
		args           []driver.Value
		queryResult    *sqlmock.Rows
		expectedResult []*model.Trace
		expectedError  error
	}{
		"JSON encoding incorrect data": {
			query:          defaultQuery,
			args:           traceIDStrings,
			queryResult:    getRows([]driver.Value{[]byte{'{', 'n', 'o', 't', '_', 'a', '_', 'k', 'e', 'y', '}'}}),
			expectedResult: []*model.Trace(nil),
			expectedError:  fmt.Errorf("invalid character 'n' looking for beginning of object key string"),
		},
		"tenant JSON encoding incorrect data": {
			tenant:         testTenant,
			query:          tenantQuery,
			args:           append(traceIDStrings, testTenant),
			queryResult:    getRows([]driver.Value{[]byte{'{', 'n', 'o', 't', '_', 'a', '_', 'k', 'e', 'y', '}'}}),
			expectedResult: []*model.Trace(nil),
			expectedError:  fmt.Errorf("invalid character 'n' looking for beginning of object key string"),
		},
		"Protobuf encoding incorrect data": {
			query:          defaultQuery,
			args:           traceIDStrings,
			queryResult:    getRows([]driver.Value{[]byte{'i', 'n', 'c', 'o', 'r', 'r', 'e', 'c', 't'}}),
			expectedResult: []*model.Trace{},
			expectedError:  nil,
		},
		"tenant Protobuf encoding incorrect data": {
			tenant:         testTenant,
			query:          tenantQuery,
			args:           append(traceIDStrings, testTenant),
			queryResult:    getRows([]driver.Value{[]byte{'i', 'n', 'c', 'o', 'r', 'r', 'e', 'c', 't'}}),
			expectedResult: []*model.Trace{},
			expectedError:  nil,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			mock.
				ExpectQuery(test.query).
				WithArgs(test.args...).
				WillReturnRows(test.queryResult)

			traceReader := NewTraceReader(spyLogger, db, testOperationsTable, testIndexTable, testSpansTable, test.tenant, testMaxNumSpans, expandTraceID)
			traces, err := traceReader.getTraces(context.Background(), traceIDs)
			if test.expectedError == nil {
				assert.NoError(t, err)
			} else {
				assert.EqualError(t, err, test.expectedError.Error())
			}
			assert.Equal(t, test.expectedResult, traces)
			assert.NoError(t, mock.ExpectationsWereMet())
		})
	}
}

func TestSpanWriter_getTracesQueryError(t *testing.T) {
	db, mock, err := mocks.GetDbMock()
	require.NoError(t, err, "an error was not expected when opening a stub database connection")
	defer db.Close()

	traceReader := NewTraceReader(spyLogger, db, testOperationsTable, testIndexTable, testSpansTable, "", testMaxNumSpans, expandTraceID)
	traceIDs := []model.TraceID{
		{High: 0, Low: 1},
		{High: 2, Low: 2},
		{High: 1, Low: 3},
		{High: 0, Low: 4},
	}

	traceIDStrings := make([]driver.Value, 4)
	for i, traceID := range traceIDs {
		traceIDStrings[i] = traceID.String()
	}

	mock.
		ExpectQuery(
			fmt.Sprintf("SELECT model FROM %s PREWHERE traceID IN (?,?,?,?)", testSpansTable),
		).
		WithArgs(traceIDStrings...).
		WillReturnError(errorMock)

	traces, err := traceReader.getTraces(context.Background(), traceIDs)
	assert.EqualError(t, err, errorMock.Error())
	assert.Equal(t, []*model.Trace(nil), traces)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestSpanWriter_getTracesRowsScanError(t *testing.T) {
	db, mock, err := mocks.GetDbMock()
	require.NoError(t, err, "an error was not expected when opening a stub database connection")
	defer db.Close()

	traceReader := NewTraceReader(spyLogger, db, testOperationsTable, testIndexTable, testSpansTable, "", testMaxNumSpans, expandTraceID)
	traceIDs := []model.TraceID{
		{High: 0, Low: 1},
		{High: 2, Low: 2},
		{High: 1, Low: 3},
		{High: 0, Low: 4},
	}

	traceIDStrings := make([]driver.Value, 4)
	for i, traceID := range traceIDs {
		traceIDStrings[i] = traceID.String()
	}
	rows := getRows([]driver.Value{"some value"}).RowError(0, errorMock)

	mock.
		ExpectQuery(
			fmt.Sprintf("SELECT model FROM %s PREWHERE traceID IN (?,?,?,?)", testSpansTable),
		).
		WithArgs(traceIDStrings...).
		WillReturnRows(rows)

	traces, err := traceReader.getTraces(context.Background(), traceIDs)
	assert.EqualError(t, err, errorMock.Error())
	assert.Equal(t, []*model.Trace(nil), traces)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestSpanWriter_getTraceNoTraceIDs(t *testing.T) {
	db, _, err := mocks.GetDbMock()
	require.NoError(t, err, "an error was not expected when opening a stub database connection")
	defer db.Close()

	traceReader := NewTraceReader(spyLogger, db, testOperationsTable, testIndexTable, testSpansTable, "", testMaxNumSpans, expandTraceID)
	traceIDs := make([]model.TraceID, 0)

	traces, err := traceReader.getTraces(context.Background(), traceIDs)
	require.NoError(t, err)
	assert.Equal(t, make([]*model.Trace, 0), traces)
}

func getEncodedSpans(spans []model.Span, marshal func(span *model.Span) ([]byte, error)) *sqlmock.Rows {
	serialized := make([]driver.Value, len(spans))
	for i := range spans {
		bytes, err := marshal(&spans[i])
		if err != nil {
			panic(err)
		}
		serialized[i] = bytes
	}
	return getRows(serialized)
}

func getRows(values []driver.Value) *sqlmock.Rows {
	rows := sqlmock.NewRows([]string{"model"})
	for _, value := range values {
		rows.AddRow(value)
	}
	return rows
}

func getTracesFromSpans(spans []model.Span) []*model.Trace {
	traces := make(map[model.TraceID]*model.Trace)
	for i, span := range spans {
		if _, ok := traces[span.TraceID]; !ok {
			traces[span.TraceID] = &model.Trace{}
		}
		traces[span.TraceID].Spans = append(traces[span.TraceID].Spans, &spans[i])
	}

	res := make([]*model.Trace, 0, len(traces))
	for _, trace := range traces {
		res = append(res, trace)
	}
	model.SortTraces(res)
	return res
}

func TestSpanWriter_findTraceIDsInRange(t *testing.T) {
	db, mock, err := mocks.GetDbMock()
	require.NoError(t, err, "an error was not expected when opening a stub database connection")
	defer db.Close()

	service := "test_service"
	operation := "test_operation"
	start := time.Unix(0, 0)
	end := time.Now()
	minDuration := time.Minute
	maxDuration := time.Hour
	tags := map[string]string{
		"key": "value",
	}
	skip := []model.TraceID{
		{High: 1, Low: 1},
		{High: 0, Low: 0},
	}
	skipHexes := make([]string, len(skip))
	for i, id := range skip {
		bts, _ := traceIDToBytes(id)
		skipHexes[i] = hex.EncodeToString(bts)
	}
	tagArgs := func(tags map[string]string) []model.KeyValue {
		res := make([]model.KeyValue, 0, len(tags))
		for key, value := range tags {
			res = append(res, model.String(key, value))
		}
		return res
	}(tags)
	rowValues := []driver.Value{
		"1",
		"2",
		"3",
	}
	rows := []model.TraceID{
		{High: 0, Low: 1},
		{High: 0, Low: 2},
		{High: 0, Low: 3},
	}
	rowsValuesNoExpand := make([]string, len(rows))
	for i, id := range rows {
		bts, _ := traceIDToBytes(id)
		rowsValuesNoExpand[i] = string(bts)
	}

	tests := map[string]struct {
		queryParams   spanstore.TraceQueryParameters
		skip          []model.TraceID
		tenant        string
		expectedQuery string
		expectedArgs  []driver.Value
		expandTraceID bool
	}{
		"default": {
			queryParams: spanstore.TraceQueryParameters{ServiceName: service, NumTraces: testNumTraces},
			skip:        make([]model.TraceID, 0),
			expectedQuery: fmt.Sprintf(
				"SELECT DISTINCT traceID FROM %s WHERE service = ? AND timestamp >= ? AND timestamp <= ? ORDER BY service, timestamp DESC LIMIT ?",
				testIndexTable,
			),
			expectedArgs: []driver.Value{
				service,
				start,
				end,
				testNumTraces,
			},
			expandTraceID: expandTraceID,
		},
		"tenant": {
			queryParams: spanstore.TraceQueryParameters{ServiceName: service, NumTraces: testNumTraces},
			skip:        make([]model.TraceID, 0),
			tenant:      testTenant,
			expectedQuery: fmt.Sprintf(
				"SELECT DISTINCT traceID FROM %s WHERE service = ? AND tenant = ? AND timestamp >= ? AND timestamp <= ? ORDER BY service, timestamp DESC LIMIT ?",
				testIndexTable,
			),
			expectedArgs: []driver.Value{
				service,
				testTenant,
				start,
				end,
				testNumTraces,
			},
			expandTraceID: expandTraceID,
		},
		"maxDuration": {
			queryParams: spanstore.TraceQueryParameters{ServiceName: service, NumTraces: testNumTraces, DurationMax: maxDuration},
			skip:        make([]model.TraceID, 0),
			expectedQuery: fmt.Sprintf(
				"SELECT DISTINCT traceID FROM %s WHERE service = ? AND timestamp >= ? AND timestamp <= ? AND durationUs <= ? ORDER BY service, timestamp DESC LIMIT ?",
				testIndexTable,
			),
			expectedArgs: []driver.Value{
				service,
				start,
				end,
				maxDuration.Microseconds(),
				testNumTraces,
			},
			expandTraceID: expandTraceID,
		},
		"minDuration": {
			queryParams: spanstore.TraceQueryParameters{ServiceName: service, NumTraces: testNumTraces, DurationMin: minDuration},
			skip:        make([]model.TraceID, 0),
			expectedQuery: fmt.Sprintf(
				"SELECT DISTINCT traceID FROM %s WHERE service = ? AND timestamp >= ? AND timestamp <= ? AND durationUs >= ? ORDER BY service, timestamp DESC LIMIT ?",
				testIndexTable,
			),
			expectedArgs: []driver.Value{
				service,
				start,
				end,
				minDuration.Microseconds(),
				testNumTraces,
			},
			expandTraceID: expandTraceID,
		},
		"tags": {
			queryParams: spanstore.TraceQueryParameters{ServiceName: service, NumTraces: testNumTraces, Tags: tags},
			skip:        make([]model.TraceID, 0),
			expectedQuery: fmt.Sprintf(
				"SELECT DISTINCT traceID FROM %s WHERE service = ? AND timestamp >= ? AND timestamp <= ?%s ORDER BY service, timestamp DESC LIMIT ?",
				testIndexTable,
				strings.Repeat(" AND has(tags.key, ?) AND has(splitByChar(',', tags.value[indexOf(tags.key, ?)]), ?)", len(tags)),
			),
			expectedArgs: []driver.Value{
				service,
				start,
				end,
				tagArgs[0].Key,
				tagArgs[0].Key,
				tagArgs[0].AsString(),
				testNumTraces,
			},
			expandTraceID: expandTraceID,
		},
		"skip": {
			queryParams: spanstore.TraceQueryParameters{ServiceName: service, NumTraces: testNumTraces},
			skip:        skip,
			expectedQuery: fmt.Sprintf(
				"SELECT DISTINCT traceID FROM %s WHERE service = ? AND timestamp >= ? AND timestamp <= ? AND traceID NOT IN (?,?) ORDER BY service, timestamp DESC LIMIT ?",
				testIndexTable,
			),
			expectedArgs: []driver.Value{
				service,
				start,
				end,
				skip[0].String(),
				skip[1].String(),
				testNumTraces - len(skip),
			},
			expandTraceID: expandTraceID,
		},
		"skip no expand traceID": {
			queryParams: spanstore.TraceQueryParameters{ServiceName: service, NumTraces: testNumTraces},
			skip:        skip,
			expectedQuery: fmt.Sprintf(
				"SELECT DISTINCT traceID FROM %s WHERE service = ? AND timestamp >= ? AND timestamp <= ? AND lower(hex(traceID)) NOT IN (?,?) ORDER BY service, timestamp DESC LIMIT ?",
				testIndexTable,
			),
			expectedArgs: []driver.Value{
				service,
				start,
				end,
				skipHexes[0],
				skipHexes[1],
				testNumTraces - len(skip),
			},
			expandTraceID: !expandTraceID,
		},
		"operation": {
			queryParams: spanstore.TraceQueryParameters{ServiceName: service, NumTraces: testNumTraces, OperationName: operation},
			skip:        make([]model.TraceID, 0),
			expectedQuery: fmt.Sprintf(
				"SELECT DISTINCT traceID FROM %s WHERE service = ? AND operation = ? AND timestamp >= ? AND timestamp <= ? ORDER BY service, timestamp DESC LIMIT ?",
				testIndexTable,
			),
			expectedArgs: []driver.Value{
				service,
				operation,
				start,
				end,
				testNumTraces,
			},
			expandTraceID: expandTraceID,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			queryResult := sqlmock.NewRows([]string{"traceID"})
			if test.expandTraceID {
				for _, row := range rowValues {
					queryResult.AddRow(row)
				}
			} else {
				for _, row := range rowsValuesNoExpand {
					queryResult.AddRow(row)
				}

			}

			mock.
				ExpectQuery(test.expectedQuery).
				WithArgs(test.expectedArgs...).
				WillReturnRows(queryResult)

			traceReader := NewTraceReader(spyLogger, db, testOperationsTable, testIndexTable, testSpansTable, test.tenant, testMaxNumSpans, test.expandTraceID)
			res, err := traceReader.findTraceIDsInRange(
				context.Background(),
				&test.queryParams,
				start,
				end,
				test.skip)
			require.NoError(t, err)
			assert.Equal(t, rows, res)
			assert.NoError(t, mock.ExpectationsWereMet())
		})
	}
}

func TestSpanReader_findTraceIDsInRangeNoIndexTable(t *testing.T) {
	db, _, err := mocks.GetDbMock()
	require.NoError(t, err, "an error was not expected when opening a stub database connection")
	defer db.Close()

	traceReader := NewTraceReader(spyLogger, db, testOperationsTable, "", testSpansTable, "", testMaxNumSpans, expandTraceID)
	res, err := traceReader.findTraceIDsInRange(
		context.Background(),
		nil,
		time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC),
		time.Date(2000, 1, 2, 0, 0, 0, 0, time.UTC),
		make([]model.TraceID, 0),
	)
	assert.Equal(t, []model.TraceID(nil), res)
	assert.EqualError(t, err, errNoIndexTable.Error())
}

func TestSpanReader_findTraceIDsInRangeEndBeforeStart(t *testing.T) {
	db, _, err := mocks.GetDbMock()
	require.NoError(t, err, "an error was not expected when opening a stub database connection")
	defer db.Close()

	traceReader := NewTraceReader(spyLogger, db, testOperationsTable, testIndexTable, testSpansTable, "", testMaxNumSpans, expandTraceID)
	res, err := traceReader.findTraceIDsInRange(
		context.Background(),
		nil,
		time.Date(2000, 1, 2, 0, 0, 0, 0, time.UTC),
		time.Date(2000, 1, 1, 0, 0, 0, 0, time.UTC),
		make([]model.TraceID, 0),
	)
	assert.Equal(t, make([]model.TraceID, 0), res)
	assert.NoError(t, err)
}

func TestSpanReader_findTraceIDsInRangeQueryError(t *testing.T) {
	db, mock, err := mocks.GetDbMock()
	require.NoError(t, err, "an error was not expected when opening a stub database connection")
	defer db.Close()

	traceReader := NewTraceReader(spyLogger, db, testOperationsTable, testIndexTable, testSpansTable, "", testMaxNumSpans, expandTraceID)
	service := "test_service"
	start := time.Unix(0, 0)
	end := time.Now()

	mock.
		ExpectQuery(fmt.Sprintf(
			"SELECT DISTINCT traceID FROM %s WHERE service = ? AND timestamp >= ? AND timestamp <= ? ORDER BY service, timestamp DESC LIMIT ?",
			testIndexTable,
		)).
		WithArgs(
			service,
			start,
			end,
			testNumTraces,
		).
		WillReturnError(errorMock)

	res, err := traceReader.findTraceIDsInRange(
		context.Background(),
		&spanstore.TraceQueryParameters{ServiceName: service, NumTraces: testNumTraces},
		start,
		end,
		make([]model.TraceID, 0))
	assert.EqualError(t, err, errorMock.Error())
	assert.Equal(t, []model.TraceID(nil), res)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestSpanReader_findTraceIDsInRangeIncorrectData(t *testing.T) {
	db, mock, err := mocks.GetDbMock()
	require.NoError(t, err, "an error was not expected when opening a stub database connection")
	defer db.Close()

	service := "test_service"
	start := time.Unix(0, 0)
	end := time.Now()

	tests := map[string]struct {
		query  string
		args   []driver.Value
		tenant string
	}{
		"default": {
			query: fmt.Sprintf(
				"SELECT DISTINCT traceID FROM %s WHERE service = ? AND timestamp >= ? AND timestamp <= ? ORDER BY service, timestamp DESC LIMIT ?",
				testIndexTable,
			),
			args: []driver.Value{service, start, end, testNumTraces},
		},
		"tenant": {
			query: fmt.Sprintf(
				"SELECT DISTINCT traceID FROM %s WHERE service = ? AND tenant = ? AND timestamp >= ? AND timestamp <= ? ORDER BY service, timestamp DESC LIMIT ?",
				testIndexTable,
			),
			args:   []driver.Value{service, testTenant, start, end, testNumTraces},
			tenant: testTenant,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			traceReader := NewTraceReader(spyLogger, db, testOperationsTable, testIndexTable, testSpansTable, test.tenant, testMaxNumSpans, expandTraceID)

			rowValues := []driver.Value{
				"1",
				"incorrect value",
				"3",
			}
			queryResult := sqlmock.NewRows([]string{"traceID"})
			for _, row := range rowValues {
				queryResult.AddRow(row)
			}
			mock.ExpectQuery(test.query).WithArgs(test.args...).WillReturnRows(queryResult)

			res, err := traceReader.findTraceIDsInRange(
				context.Background(),
				&spanstore.TraceQueryParameters{ServiceName: service, NumTraces: testNumTraces},
				start,
				end,
				make([]model.TraceID, 0))
			assert.Error(t, err)
			assert.Equal(t, []model.TraceID(nil), res)
			assert.NoError(t, mock.ExpectationsWereMet())
		})
	}
}

func TestSpanReader_getStrings(t *testing.T) {
	db, mock, err := mocks.GetDbMock()
	require.NoError(t, err, "an error was not expected when opening a stub database connection")
	defer db.Close()

	query := "SELECT b FROM a WHERE b != ?"
	argValues := []driver.Value{driver.Value("a")}
	args := []interface{}{"a"}
	rows := []driver.Value{"some", "query", "rows"}
	expectedResult := []string{"some", "query", "rows"}
	result := sqlmock.NewRows([]string{"b"})
	for _, str := range rows {
		result.AddRow(str)
	}
	mock.ExpectQuery(query).WithArgs(argValues...).WillReturnRows(result)

	traceReader := NewTraceReader(spyLogger, db, testOperationsTable, testIndexTable, testSpansTable, "", testMaxNumSpans, expandTraceID)

	queryResult, err := traceReader.getStrings(context.Background(), query, args...)
	assert.NoError(t, err)
	assert.EqualValues(t, expectedResult, queryResult)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestSpanReader_getStringsQueryError(t *testing.T) {
	db, mock, err := mocks.GetDbMock()
	require.NoError(t, err, "an error was not expected when opening a stub database connection")
	defer db.Close()

	query := "SELECT b FROM a WHERE b != ?"
	argValues := []driver.Value{driver.Value("a")}
	args := []interface{}{"a"}
	mock.ExpectQuery(query).WithArgs(argValues...).WillReturnError(errorMock)

	traceReader := NewTraceReader(spyLogger, db, testOperationsTable, testIndexTable, testSpansTable, "", testMaxNumSpans, expandTraceID)

	queryResult, err := traceReader.getStrings(context.Background(), query, args...)
	assert.EqualError(t, err, errorMock.Error())
	assert.EqualValues(t, []string(nil), queryResult)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestSpanReader_getStringsRowError(t *testing.T) {
	db, mock, err := mocks.GetDbMock()
	require.NoError(t, err, "an error was not expected when opening a stub database connection")
	defer db.Close()

	query := "SELECT b FROM a WHERE b != ?"
	argValues := []driver.Value{driver.Value("a")}
	args := []interface{}{"a"}
	rows := []driver.Value{"some", "query", "rows"}
	result := sqlmock.NewRows([]string{"b"})
	for _, str := range rows {
		result.AddRow(str)
	}
	result.RowError(2, errorMock)
	mock.ExpectQuery(query).WithArgs(argValues...).WillReturnRows(result)

	traceReader := NewTraceReader(spyLogger, db, testOperationsTable, testIndexTable, testSpansTable, "", testMaxNumSpans, expandTraceID)

	queryResult, err := traceReader.getStrings(context.Background(), query, args...)
	assert.EqualError(t, err, errorMock.Error())
	assert.EqualValues(t, []string(nil), queryResult)
	assert.NoError(t, mock.ExpectationsWereMet())
}
