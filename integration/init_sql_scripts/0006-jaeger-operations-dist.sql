CREATE TABLE IF NOT EXISTS jaeger_operations ON CLUSTER '{cluster}' AS default.jaeger_operations_local
ENGINE = Distributed('{cluster}', 'default', jaeger_operations_local, cityHash64(tenant));