CREATE TABLE IF NOT EXISTS jaeger_spans_local ON CLUSTER '{cluster}'
(
    `tenant` LowCardinality(String),
    `timestamp` DateTime64(6, 'UTC') CODEC(Delta(4), ZSTD(1)), -- Precision till microseconds
    `traceID` FixedString(16),
    -- model has the full encoded span
    `model` String CODEC(ZSTD(3))
)
ENGINE = ReplicatedMergeTree -- MergeTree params are omitted because they are controlled via clickhouse-operator
PARTITION BY toYYYYMM(timestamp)
PRIMARY KEY(tenant, traceID)
ORDER BY (tenant, traceID, timestamp)
TTL toDateTime(timestamp) + toIntervalDay(30);
