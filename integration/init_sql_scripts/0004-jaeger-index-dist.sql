CREATE TABLE IF NOT EXISTS jaeger_index ON CLUSTER '{cluster}' AS default.jaeger_index_local
    ENGINE = Distributed('{cluster}', 'default', jaeger_index_local, cityHash64(tenant, traceID));