CREATE TABLE IF NOT EXISTS jaeger_spans ON CLUSTER '{cluster}' AS default.jaeger_spans_local
    ENGINE = Distributed('{cluster}', 'default', jaeger_spans_local, cityHash64(tenant, traceID));