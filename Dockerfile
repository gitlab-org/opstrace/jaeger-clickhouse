FROM --platform=${BUILDPLATFORM} golang:1.19 AS build-env
ENV CGO_ENABLED=0
ENV GOPATH=/go

ARG TARGETARCH
ARG TARGETOS

WORKDIR /workspace/jaeger-clickhouse

# Cache dependencies
COPY go.mod ./
COPY go.sum ./
RUN go mod download -x

COPY . .

RUN ["/bin/bash", "-c", "GOOS=${TARGETOS} GOARCH=${TARGETARCH} go build -trimpath -o jaeger-clickhouse-${TARGETOS}-${TARGETARCH} ./cmd/jaeger-clickhouse/main.go"]

# NOTE: We can't switch to distroless here as the jaeger relies on coping the
# plugin into it's container. We could in theory provide a copy `cp` command
# and copy the missing libraries into the static container, but this seems like
# an overkill as the container itself is used only during the initialization
FROM docker.io/library/alpine:latest
ARG TARGETARCH
ARG TARGETOS
COPY --from=build-env /workspace/jaeger-clickhouse/jaeger-clickhouse-${TARGETOS}-${TARGETARCH} /go/bin/jaeger-clickhouse

RUN mkdir /plugin

# /plugin/ location is defined in jaeger-operator
CMD ["cp", "/go/bin/jaeger-clickhouse", "/plugin/jaeger-clickhouse"]
