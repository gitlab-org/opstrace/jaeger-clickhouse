package testutils

import (
	"context"
	"fmt"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
)

const (
	ClickHousePort = "9000/tcp"
)

func CreateClickHouseContainer(ctx context.Context, image string, configPath *string, networks []string) (testcontainers.Container, error) {
	var mounts testcontainers.ContainerMounts
	if configPath != nil {
		mounts = testcontainers.Mounts(testcontainers.BindMount(*configPath, "/etc/clickhouse-server/config.d/testconf.xml"))
	}
	chReq := testcontainers.ContainerRequest{
		Image:        image,
		ExposedPorts: []string{ClickHousePort},
		WaitingFor:   &clickhouseWaitStrategy{pollInterval: time.Millisecond * 200, startupTimeout: time.Minute},
		Networks:     networks,
		Hostname:     "chi",
		Mounts:       mounts,
	}
	return testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
		ContainerRequest: chReq,
		Started:          true,
	})
}

type clickhouseWaitStrategy struct {
	pollInterval   time.Duration
	startupTimeout time.Duration
}

var _ wait.Strategy = (*clickhouseWaitStrategy)(nil)

func (c *clickhouseWaitStrategy) WaitUntilReady(ctx context.Context, target wait.StrategyTarget) error {
	ctx, cancelContext := context.WithTimeout(ctx, c.startupTimeout)
	defer cancelContext()

	port, err := target.MappedPort(ctx, ClickHousePort)
	if err != nil {
		return err
	}

	host, err := target.Host(ctx)
	if err != nil {
		return err
	}

	db := clickhouse.OpenDB(&clickhouse.Options{
		Addr: []string{
			fmt.Sprintf("%s:%d", host, port.Int()),
		},
		Auth: clickhouse.Auth{
			Database: "default",
		},
		Compression: &clickhouse.Compression{
			Method: clickhouse.CompressionLZ4,
		},
	})
	defer db.Close()

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case <-time.After(c.pollInterval):
			if err := db.Ping(); err != nil {
				continue
			}
			return nil
		}
	}
}
